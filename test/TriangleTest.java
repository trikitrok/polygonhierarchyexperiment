import static org.junit.Assert.fail;

import org.junit.Test;

public class TriangleTest {

	@Test(expected = NotEnoughVerticesException.class)
	public void trianglesHaveThreeDifferentVertices() {
		PolygonsFactory.createTriangle(new Vertex(1.0, 0.0), new Vertex(0.0, 0.0), new Vertex(0.0, 0.0));
	}

	@Test
	public void aRealTriangle() {
		try {
			PolygonsFactory.createTriangle(new Vertex(1.0, 0.0), new Vertex(0.0, 0.0), new Vertex(0.0, 2.0));
		} catch (NotEnoughVerticesException e) {
			fail("it was a triangle");
		}
	}
}
