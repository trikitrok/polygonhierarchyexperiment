import static org.junit.Assert.*;

import org.junit.Test;

public class RectangleTest {

	@Test(expected = NotEnoughVerticesException.class)
	public void rectanglesHaveFourDifferentVertices() {
		PolygonsFactory.createRectangle(new Vertex(1.0, 0.0), new Vertex(0.1,5.0), 
										new Vertex(0.0, 0.0), new Vertex(0.0, 0.0));
	}

	@Test(expected = NotRectangleException.class)
	public void rectanglesHaveEqualOppositeSides() {
		PolygonsFactory.createRectangle(new Vertex(0.0, 0.0), new Vertex(0.0, 1.0), 
										new Vertex(1.0, 1.0), new Vertex(2.0, 0.0));
	}

	@Test(expected = NotRectangleException.class)
	public void squaresAreNotRectangles() {
		PolygonsFactory.createRectangle(new Vertex(0.0, 0.0), new Vertex(0.0, 1.0), 
										new Vertex(1.0, 1.0), new Vertex(1.0, 0.0));
	}

	@Test
	public void aRealRectangle() {
		try {
			PolygonsFactory.createRectangle(new Vertex(0.0, 0.0), new Vertex(0.0, 2.0), 
											new Vertex(1.0, 2.0), new Vertex(1.0, 0.0));
		} catch (NotEnoughVerticesException e) {
			fail("it was a rectangle..");
		} catch (NotRectangleException e) {
			fail("it was a rectangle..");
		}
	}
}
