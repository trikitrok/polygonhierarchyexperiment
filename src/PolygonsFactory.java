public class PolygonsFactory {

	public static Triangle createTriangle(Vertex vertex1, Vertex vertex2, Vertex vertex3) {
		return new Triangle(vertex1, vertex2, vertex3);
	}

	public static Rectangle createRectangle(Vertex vertex1, Vertex vertex2, Vertex vertex3, Vertex vertex4) {
		return new Rectangle(vertex1, vertex2, vertex3, vertex4);
	}
	
	public static Square createSquare(Vertex vertex1, Vertex vertex2, Vertex vertex3, Vertex vertex4) {
		return new Square(vertex1, vertex2, vertex3, vertex4);
	}
}
