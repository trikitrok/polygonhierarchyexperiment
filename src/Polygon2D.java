import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public abstract class Polygon2D {

	protected List<Side> sides;

	public Polygon2D(Vertex... vertices) {
		Vertex[] differentVertices = filterTheRepeated(vertices);

		if (notEnough(differentVertices))
			throw new NotEnoughVerticesException();

		createSides(differentVertices);
	}

	public boolean isRegular() {

		boolean sidesAreEqual = false;

		Side firstSide = sides.get(0);

		for (int i = 1; i < sides.size(); ++i) {
			Side nextSide = sides.get(i);

			sidesAreEqual = firstSide.hasSameLength(nextSide);

			if (!sidesAreEqual)
				break;
		}

		return sidesAreEqual;
	}

	private Vertex[] filterTheRepeated(Vertex... vertices) {
		Set<Vertex> differentVertices = new LinkedHashSet<Vertex>();

		for (Vertex vertex : vertices)
			differentVertices.add(vertex);

		return differentVertices.toArray(new Vertex[0]);
	}

	private boolean notEnough(Vertex[] vertices) {
		return vertices.length < 3;
	}

	private void createSides(Vertex[] vertices) {
		this.sides = new ArrayList<Side>();

		for (int i = 1; i < vertices.length; ++i) {
			this.sides.add(new Side(vertices[i - 1], vertices[i]));
		}

		this.sides.add(new Side(vertices[vertices.length - 1], vertices[0]));
	}

	protected int numberOfSides() {
		return sides.size();
	}
}
