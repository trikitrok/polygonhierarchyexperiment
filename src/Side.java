public class Side {
	private double length;

	public Side(Vertex vertex1, Vertex vertex2) {
		this.length = vertex1.distance(vertex2);
	}

	public boolean hasSameLength(Side other) {
		return NumbersUtils.areEqual(length, other.length);
	}
}
